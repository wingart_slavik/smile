$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var homeSlider  		= $(".home-slider"),
	technologiesSlider  = $(".technologies-slider"),
	equalH      		= $('.maxheight'),
	popup 				= $("[data-popup]"),
	matchheight 		= $("[data-mh]");

if(homeSlider.length || technologiesSlider.length){
  include("js/swiper.jquery.js");
}

if(popup.length){
	include('js/jquery.arcticmodal.js');
}

if (equalH.length){
	include("js/jquery.equalheights.js");
}

if(matchheight.length){
  include("js/jquery.matchHeight-min.js");
}


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}



$(document).ready(function(){

	if(homeSlider.length){
		var swiper = new Swiper('.home-slider', {
		    pagination: false,
		    paginationClickable: false,
		    nextButton: '.home-slider-button-next',
		    prevButton: '.home-slider-button-prev',
		    speed : 1200,
		    parallax : true
		});
	}

	if(technologiesSlider.length){
		var swiper = new Swiper('.technologies-slider', {
		    pagination: true,
		    paginationClickable: true,
		    nextButton: '.technologies-slider-button-next',
		    prevButton: '.technologies-slider-button-prev',
		    pagination: '.technologies-pagination',
		    speed : 1200,
		    parallax : true
		});
	}

	if(popup.length){
		popup.on('click',function(){
		    var modal = $(this).data("popup");
		    $(modal).arcticmodal();
		});
	};

	$(".navigation_btn, .nav_overlay").on("click", function(){
	  	$("body").toggleClass("navTrue");
	})


})